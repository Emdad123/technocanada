package Tests;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.Cart;
import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class CartTest extends TechnoTutorBase 
{

	private static Home home;
	private static Cart cart;
	private static ExtentReports reports;
	private static ExtentTest test;
	
	

	@BeforeClass
	public void init() 
	{
		initilization();
		home = PageFactory.initElements(driver, Home.class);
		cart = PageFactory.initElements(driver, Cart.class);
		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate Cart Page Test Cases");
		home.linkCart.click();

	}
	
	
	@Test(priority = 1, enabled=true)
	public void title() 
	{
		
		
		// TITLE OF CART PAGE
		String Title = driver.getTitle();
		Assert.assertEquals(Title, "Cart � Techno Tutors", "Title is Incorrect");
		LogWithScreenshot.logStatusWithScreenShot(driver, test, " Cart Page Title", LogStatus.PASS,
				"Cart Page Title TC  : Passed");
		 


		// MAIN CART PAGE CLICK ON RETURN TO CHECK IT'S NAVIGATING BACK AND ADDING ITEMS
		cart.ReturntoShop.click();
		

		// PRINTING TITLE OF SHOP PAGE
		Assert.assertEquals(driver.getTitle(), "Products � Techno Tutors", "Title is Incorrect");
		LogWithScreenshot.logStatusWithScreenShot(driver, test, "Shop Page Title", LogStatus.PASS,
				" Shop Page Title TC  : Passed");

	}
	
	@AfterClass
	public static void teardown() 
	{

		reports.endTest(test);
		reports.flush();
		driver.quit();

	}


}
