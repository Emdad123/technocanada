package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.ContactUs;
import Pages.Facility;
import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class ContactUsTest extends TechnoTutorBase {
	
	 private static Home home;
	 private static ContactUs contactus;
	 private static Facility facility;
	 private static ExtentReports reports;
	 private static ExtentTest test;

	

	@BeforeClass
	public void init() 
	{
		initilization();
		home = PageFactory.initElements(driver, Home.class);
		contactus = PageFactory.initElements(driver, ContactUs.class);
		facility = PageFactory.initElements(driver, Facility.class);
		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate ContactUs Page Test Cases");
		home.linkContact.click();
	}
	

	// CONTACT US TITLE TEST
	@Test(priority = 1, enabled = true)
	public void checktitle() {
		String Title = driver.getTitle();
		Assert.assertEquals(Title, "Contact � Techno Tutors", "Title is Incorrect");
		LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Title", LogStatus.PASS,
				"Contact Us Title TC  : Passed");
	}

	// CONTACTS US LOGO ,TEXT, EMAIL AND PHONE NUMBER TEST
	@Test(priority = 2, enabled = true)
	public void logoandtext() {
		if (contactus.logocontactus.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Logo Display", LogStatus.PASS,
					"ContactUs Page Logo TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Logo Display", LogStatus.PASS,
					"ContactUs Page Logo TC : Failed");
		}
		if (contactus.adresstext.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Adress Display", LogStatus.PASS,
					"ContactUs Page Adress TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Adress Display", LogStatus.PASS,
					"ContactUs Page Adress TC : Failed");
			
		}
		if (contactus.emailtext.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Email Display", LogStatus.PASS,
					"ContactUs Page Email TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Email Display", LogStatus.PASS,
					"ContactUs Email Adress TC : Failed");
		}
		if (contactus.phonenumtext.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us PhoneNumber Display", LogStatus.PASS,
					"ContactUs Page PhoneNumber TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us PhoneNumber Display", LogStatus.PASS,
					"ContactUs PhoneNumber Adress TC : Failed");
		}

	}

	// MANDATORY FIELD DATA FOR CONTACT US
	@Test(priority = 3, enabled = true)
	public void getInTouchdata() {
		contactus.getInTouch("Vijay", "Vijay@gmail.com", "Maths Acadamic", "Information");

		if (facility.lblSuccessMessage.isDisplayed()) {
			
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch With Data", LogStatus.PASS,
					"ContactUs Page Mandatory Data For Get In Touch TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch With Data", LogStatus.PASS,
					"ContactUs Page Mandatory Data For Get In Touch TC : Failed");
		
		}
	}

	// MANDATORY FIELD ERROR TEST
	@Test(priority = 4, enabled = true)
	public void getInToucherror() {
		contactus.getInTouch("", "", "", "");
		if (facility.ERMessageName.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Name Error TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Name Error TC : Failed");
		
			}

		if (facility.ERMessageEmail.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Email Error TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Email Error TC : Failed");
		
			}
		if (facility.ERMessageSubject.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Subject Error TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Subject Error TC : Failed");
		
			}

		if (facility.ErrMessageMessage.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Message Error TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Message Error TC : Failed");
		
			}
		if (facility.ErrMessageSend.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Send Error TC : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Contact Us Get In Touch Without Data", LogStatus.PASS,
					"ContactUs Page Send Error TC : Failed");
		
			}

	}

	

	@AfterClass
	public void teardown() 
	{
		reports.endTest(test);
		reports.flush();
		driver.quit();
	}
}
