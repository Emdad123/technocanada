package Tests;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.BookaTutor;
import Pages.Home;
import Pages.SubjectAreas;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class BookaTutorTest extends TechnoTutorBase 
{

	Home home;
	BookaTutor bookatutor;
	static ExtentTest test;
	private static ExtentReports reports;


	// Report Initialization
	@BeforeClass
	public void init() 
	{
		initilization();

		home = PageFactory.initElements(driver, Home.class);
		bookatutor = PageFactory.initElements(driver, BookaTutor.class);

		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate Book a Tutor Test Cases");
		home.linkSubjectAreas.click();

	}

	// Math Learner Package Title
	@Test(priority = 1,enabled=true)
	public void MathBookaTutorLearnertitle() 
	{
		// Open 'Book a Tutor' Link for Math.
		bookatutor.linkBookaTutorM.click();

		// Verify the Learner Package Title
	     if(bookatutor.titleLearnerPkgM.isDisplayed())
	     {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathBookaTutorLearner", LogStatus.PASS,
					"Learner Package Title is displayed: Passed");
		 }

	     else 
	     {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathBookaTutorLearner", LogStatus.PASS,
					"Learner Package Title is displayed: Failed");
		}

	}

	// Math Learner Package price

	@Test(priority = 2,enabled=true)
	public void MathLearnerprice() 
	{

		bookatutor.quantityLearnerM.clear();
		bookatutor.quantityLearnerM.sendKeys("2");
		bookatutor.buttonAddtocartLearnerM.click();

		// scroll-down for Screenshot
		JavascriptExecutor je = (JavascriptExecutor) driver;

		je.executeScript("arguments[0].scrollIntoView(true);", bookatutor.link$LearnerM);

		if (bookatutor.link$LearnerM.isDisplayed()) 
		{

			// Assert.assertEquals(bookatutor.link$LearnerM.getText(), "$240.00");

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathLearnerPrice", LogStatus.PASS,
					"Price for Math Learner:(2)Total:$240.00 is displayed: Passed");
		}

		else 
		{

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathLearnerPrice", LogStatus.FAIL,
					"Price for Math Learner:(2)Total:$240.00 is displayed: Failed");
		}

		// Open the View Cart link
		bookatutor.linkViewCart.click();

		// Verify Cart Page with 'Cart' Title

		Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
		bookatutor.emptycart.click();
		
	    driver.navigate().back();

	}

	// Math Academic Package

	@Test(priority = 3,enabled=true)
	public void MathBookaTutorAcademic() 
	{

		// Verify the Academic Package Title
		if (bookatutor.titleAcademicPkgM.isDisplayed())
		{
			test.log(LogStatus.PASS,"Showing Academic Package Title");
		}
		else 
		{
			test.log(LogStatus.PASS,"Academic Package Title is not found");
		}

		// Verify Quantity Text Box

		bookatutor.quantityAcademicM.clear();
		bookatutor.quantityAcademicM.sendKeys("3");

		// Add Math Academic Package in Cart
		bookatutor.buttonAddtocartAcademicM.click();

		// scroll-down for Screenshot
		JavascriptExecutor je = (JavascriptExecutor) driver;

		je.executeScript("arguments[0].scrollIntoView(true);", bookatutor.link$AcademicM);

		if (bookatutor.link$AcademicM.isDisplayed()) {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathAcademicPrice", LogStatus.PASS,
					"Price for Math Academic Package:(3)Total:$540.00 is displayed: Passed");
		}

		else {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "MathAcademicPrice", LogStatus.FAIL,
					"Price for Math Academic Package:(3)Total:$540.00 is displayed: Failed");
		}

		// Open the View Cart Link

		bookatutor.linkViewCart.click();

		// Verify Cart Page with 'Cart' Title

		System.out.println("The Page with Title  " + bookatutor.clickViewCart.getText() + " " + "is displayed");

		Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
		bookatutor.emptycart.click();
		
		home.linkSubjectAreas.click();
		

	}

	// English Learner Package

	@Test(priority = 4,enabled=true)
	public void EnglishBookaTutorLearner()
	{
		

		// Open Book a Tutor Link for English

		bookatutor.linkBookaTutorE.click();

		// Verify Quantity Text Box
		bookatutor.quantityLearnerE.clear();
		bookatutor.quantityLearnerE.sendKeys("4");

		// Add English Learner Package in Cart
		bookatutor.buttonAddtocartLearnerE.click();

		// scroll-down for Screenshot
		JavascriptExecutor je = (JavascriptExecutor) driver;

		je.executeScript("arguments[0].scrollIntoView(true);", bookatutor.link$LearnerE);

		if (bookatutor.link$LearnerE.isDisplayed()) {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishLearnerPrice", LogStatus.PASS,
					"Price for English Learner:(4)Total:$480.00 is displayed: Passed");
		}

		else {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishLearnerPrice", LogStatus.FAIL,
					"Price for English Learner:(4)Total:$480.00 is displayed: Failed");
		}

		// Open the View Cart Link

		bookatutor.linkViewCart.click();

		// Verify Cart Page with 'Cart' Title
		

		Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
		bookatutor.emptycart.click();
		driver.navigate().back();
		
	}

	// English Academic Package

	@Test(priority = 5,enabled=true)
	public void EnglishBookaTutorAcademic()  
	{

		// Verify the Academic Package Title

		try {
			Assert.assertEquals(bookatutor.titleAcademicPkgE.getText(), "Academic Package");

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishBookaTutorAcademic", LogStatus.PASS,
					"English Academic Package Title is displayed: Passed");
		}

		catch (Exception e) {

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishBookaTutorAcademic", LogStatus.FAIL,
					"English Academic Package Title is displayed: Failed");
		}

		// Validate Quantity Text Box
		bookatutor.quantityAcademicE.clear();
		bookatutor.quantityAcademicE.sendKeys("3");

		// Add English Academic Package in Cart
		bookatutor.buttonAddtocartAcademicE.click();
		if (bookatutor.link$AcademicE.isDisplayed()) 
		{

						LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishAcademicPrice", LogStatus.PASS,
						"Price for English Learner:(4)Total:$540.00 is displayed: Passed");
			}

			else {

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "EnglishAcademicPrice", LogStatus.FAIL,
						"Price for English Learner:(4)Total:$540.00 is displayed: Failed");
			}

		// Open the View Cart Link
		bookatutor.linkViewCart.click();

		// Verify Cart Page with 'Cart' Title

		Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
		bookatutor.emptycart.click();
		home.linkSubjectAreas.click();
		
	}

	// French Learner Package

	@Test(priority = 6,enabled=true)
	public void FrenchBookaTutorLearner() 
    {

		try {

			// Open 'Book a Tutor' Link for French
			bookatutor.linkBookaTutorF.click();

			if (bookatutor.titleLearnerPkgF.isDisplayed())
			{
				test.log(LogStatus.PASS, "Showing Learner Package Title");
			}
			else {
				test.log(LogStatus.FAIL,"Learner Package Title is not found");
			}

			// Validate Quantity Text Box

			bookatutor.quantityLearnerF.clear();
			bookatutor.quantityLearnerF.sendKeys("2");

			// Add French Learner Package in Cart

			bookatutor.buttonAddtocartLearnerF.click();

			// scroll-down for Screenshot
			JavascriptExecutor je = (JavascriptExecutor) driver;

			je.executeScript("arguments[0].scrollIntoView(true);", bookatutor.link$LearnerF);

			if (bookatutor.link$LearnerF.isDisplayed()) {

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "FrenchLearnerPrice", LogStatus.PASS,
						"French Learner Package Price:(2)Total:$240.00 is displayed: Passed");
			}

			else {

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "FrenchLearnerPrice", LogStatus.FAIL,
						"French Learner Package Price:(2)Total:$240.00 is displayed: Failed");
			}

			// Open the View Cart Link

			bookatutor.linkViewCart.click();

			// Verify Cart Page with 'Cart' Title

			Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
			bookatutor.emptycart.click();
			driver.navigate().back();
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// French Academic Package

	@Test(priority = 7,enabled=true)
	public void FrenchBookaTutorAcademic() 
	{
		

		try {


			
			// Verify the Academic Package Title

			if (bookatutor.titleAcademicPkgF.isDisplayed())
			{
				LogWithScreenshot.logStatusWithScreenShot(driver, test, "FrenchAcademicTitle", LogStatus.PASS,
						"French Academic Package Title is displayed: Passed");
			}

			else {
				LogWithScreenshot.logStatusWithScreenShot(driver, test, "FrenchAcademicTitle", LogStatus.FAIL,
						"French Academic Package Title is displayed: Failed");

			}

			// Validate the Quantity Text Box

			bookatutor.quantityAcademicF.clear();
			bookatutor.quantityAcademicF.sendKeys("3");

			// Add French Academic Package in Cart
			bookatutor.buttonAddtocartAcademicF.click();

			// Open the 'View Cart' Link
			bookatutor.linkViewCart.click();

			// Verify Cart Page with 'Cart' Title
			Assert.assertEquals(bookatutor.clickViewCart.getText(), "Cart");
			bookatutor.emptycart.click();
			driver.navigate().back();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test(priority = 8,enabled=true)
	public void quantityError() {

		

		try {

			// Open 'Book a Tutor' Link for French

		
			// Validate the Quantity Text Box

			bookatutor.quantityAcademicF.clear();
			bookatutor.quantityAcademicF.sendKeys("0");

			// Add French Academic Package in Cart
			bookatutor.buttonAddtocartAcademicF.click();

			// Open the 'View Cart' Link
			bookatutor.linkViewCart.click();

						Assert.assertEquals(bookatutor.quantityError.getText(), "Your cart is currently empty.");

			if (bookatutor.quantityError.isDisplayed()) {

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "Quantity Error", LogStatus.PASS,
						"Your cart is currently empty message is displayed: Passed");
			}

			else {

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "Quantity Error", LogStatus.FAIL,
						"Your cart is currently empty message is displayed: Failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public void teardown() 
	{
		reports.endTest(test);
		reports.flush();
		driver.quit();
	}

}