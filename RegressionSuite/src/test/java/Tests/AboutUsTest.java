package Tests;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.AboutUs;
import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

import java.util.Set;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class AboutUsTest extends TechnoTutorBase
{
	//About Us Page Test Scripts
	private static Home home;
	private static AboutUs about;
	private static ExtentReports reports;
	private static ExtentTest test;
	

	//Driver and URL initialization
	//Report Intialization
	@BeforeClass
	  private static void init() 
	  {
		 initilization();
		 home = PageFactory.initElements(driver, Home.class);
		 about = PageFactory.initElements(driver, AboutUs.class);
		 reports = ExtentFactory.getReporterInstance();
		 test = reports.startTest("Verify About Us Page Test Cases");
	  }
	
		
	@Test(priority = 1, enabled=true)
	void ValidateContactUsButtonPresence()
	{	
		//Verify CONTACT US button should be displayed on About Us page
		home.linkaboutUs.click();
		Assert.assertEquals(driver.getTitle(), "About Us � Techno Tutors");
		if(about.btnContactUS.isDisplayed() == true)
		{
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsValidateContactUsButtonPresence", LogStatus.PASS, "Contact Us Button Display :: About Us Page TC : Passed");
		}
		else
		{
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsValidateContactUsButtonPresence", LogStatus.FAIL, "Contact Us Button Display :: About Us Page TC : Failed");
		}
	}	
	
	
	@Test(priority =3, enabled=true)
	void ValidateContactUsPopupSendInquiryTC()
	{	
		//Verify Send Message functionality CONTACT US  popup 
		if(about.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			about.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				sendInquiry("Ganesh", "Ganesh123@gmail.com", "Inquiry", "Course Inquiry"); 
				if(about.lblSuccessMessage.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsValidateContactUsPopupSendInquiryTC", LogStatus.PASS, "Contact Us Popup of About Us Page : Send functionality TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsValidateContactUsPopupSendInquiryTC", LogStatus.FAIL, "Contact Us Popup of About Us Page :Send functionality TC: Failed");
				}
			}
			about.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
		}
	}	
	
	
	@Test(priority = 2, enabled=true)
	void VerifyNameTC()
	{	
		//Validate Name textbox of CONTACT US  popup  should have letters only 
		if(about.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			about.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				String name ="mani";
				String regexString = "[A-Za-z]";//"^[A-Z+$]";
				sendInquiry(name, "Mani123@gmail.com", "FeeInquiry", "Inquiry about course Fee");
				if (name.matches("[" + regexString + "]+"))
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsVerifyNameTC", LogStatus.PASS, "Contact Us Popup : About Us Page : Name field accepting only letters TC : Passed"); 
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsVerifyNameTC", LogStatus.FAIL, "Contact Us Popup : About Us Page : Name field accepting only letters TC : Failed"); 
				}
			}
			about.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
		}
 	 }
	 
	
	 @Test(priority = 4, enabled=true)
	 void VerifyEmailTC()
	 {	
		//Validate the email field of CONTACT US  popup 
		if(about.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			about.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				sendInquiry("Anuja", "anuja23gmailcom", "CourseInquiry", "Inquiry about course of selenium");
				try
				{
					if (HomeTest.isValidEmail(about.txtEmail.getText())==false && about.ERMessageWrongEmail.isDisplayed()) 
					{
	        
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsVerifyEmailTC", LogStatus.PASS, "Contact Us Popup : About Us Page : Email Address Validation TC : Passed"); 
					}
				}
				catch(Exception e)
				{
					e.getMessage();
					//LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsVerifyEmailTC", LogStatus.FAIL, "Contact Us Popup : About Us Page : Email Address Validation TC : Failed");
				}
			}
			about.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
		}
	 }
	
	
	@Test(priority = 5, enabled=true)
	  void MandatoryFieldsTC()
	  {	
		 	//Verify Name, Email and Subject of CONTACT US Section are manadatory fields 
			if(about.btnContactUS.isDisplayed() == true)
			{								
			String parentID = driver.getWindowHandle();
			about.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				sendInquiry("", "", "", "");
				if(about.ERMessageName.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCName", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Name Field TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCName", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Name Field TC : Failed");
	 	    	
				}
				if(about.ERMessageEmail.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCEmail", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Email Field TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCEmail", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Email Field TC : Failed");
				}
				if(about.ERMessageSubject.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCSubject", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Subject Field TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCSubject", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Subject Field TC : Failed");
				}
		 	
				if(about.ErrMessageMessage.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCMessage", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Message Field TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCMessage", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Message Field TC : Failed");
				}
				if(about.ErrMessageSend.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCSend", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Fields TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUsMandatoryFieldsTCSend", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Fields TC : Failed");
				}
		 
			}
			about.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
			home.linkHome.click();
			test.log(LogStatus.PASS, "Page redirected to Home Page");
		}
	 }
	
	 //method for entering data to name, email, subject and message fields of Contact Us section/Popup
	 void sendInquiry(String name, String email, String subject, String message) 
	 {
		
		 about.txtName.sendKeys(name);
		 about.txtEmail.sendKeys(email);
		 about.txtSubject.sendKeys(subject);
		 about.txtMessage.sendKeys(message);
		 about.btnSend.click();
		 try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 
	 //ending report
	//Closing Browser
	 @AfterClass
	 public static void teardown() 
	 {
		 reports.endTest(test);
		 reports.flush();
		 driver.close();
	 }
	 
	 
}
