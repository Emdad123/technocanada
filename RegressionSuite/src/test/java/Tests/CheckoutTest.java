package Tests;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.Cart;
import Pages.Checkout;
import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class CheckoutTest extends TechnoTutorBase 
{

	private static Home home;
	private static Cart cart;
	private static Checkout checkout;
	private static ExtentReports reports;
	private static ExtentTest test;

	@BeforeClass
	public void init() 
	{
		initilization();
		home = PageFactory.initElements(driver, Home.class);
		cart = PageFactory.initElements(driver, Cart.class);
		checkout = PageFactory.initElements(driver, Checkout.class);
		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate CheckOut Page Test Cases");
		home.linkCart.click();

	}
	

	// RETURNING STUDENTS GET DISCOUNTS
	@Test(priority = 1)
	public void returningstudent() {

		// MAIN CART PAGE CLICK ON RETURN TO CHECK IT'S NAVIGATING BACK AND ADDING ITEMS
		cart.ReturntoShop.click();

		// Add ACADAMIC IN CART
		cart.shop();

		// CHECKOUT PRODUCTS
		cart.CheckOutButton.click();
		

		// RETURNING CUSTOMER
		checkout.ReturningCustomer.click();
		checkout.UserName.sendKeys("abc");
		checkout.Password.sendKeys("abc");
		checkout.RememberMe.click();
		checkout.send.click();
		if (checkout.invallidemail.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Login", LogStatus.PASS,
					"Checkout Page Returning Students Login TC  : Passed");
		}
		else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Login", LogStatus.PASS,
					"Checkout Page Returning Students Login TC : Failed");
		}

	}

	@Test(priority = 2)
	public void Coupon() {

		checkout.couponlink.click();

		// CHECK FOR COUPONS
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(checkout.applycheckoutcoupon));

		checkout.applycheckoutcoupon.click();

		if (checkout.couponerror.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Coupon", LogStatus.PASS,
					"Checkout Page Returning Students Coupon TC  : Passed");
		}
		else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Coupon", LogStatus.PASS,
					"Checkout Page Returning Students Coupon TC : Failed");
		}

		try {
			checkout.couponlink.click();
			checkout.insertcoupon.sendKeys("abc");
			checkout.applycheckoutcoupon.click();
			
			if (checkout.wrongecouponerror.isDisplayed()) {
				LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Coupon", LogStatus.PASS,
						"Checkout Page Returning Students CouponError TC  : Passed");
			}
			else {
				LogWithScreenshot.logStatusWithScreenShot(driver, test, "Returning Students Coupon", LogStatus.PASS,
						"Checkout Page Returning Students CouponError TC : Failed");
			}
			
		} catch (Exception e) {
			e.getMessage();
		}

	}

	@Test(priority = 3)
	public void placeOrder() {

		// BILLING INFORMATION ERRORS
		checkout.Firstname.sendKeys("abc");
		checkout.Lastname.sendKeys("xyz");
		checkout.Comapany.sendKeys("abc");
		checkout.Streetname.sendKeys("abc");
		checkout.apparment.sendKeys("abc");
		checkout.Town.sendKeys("abc");
		checkout.Province.click();
		checkout.InputProvince.sendKeys("Ontario");
		checkout.postalcode.sendKeys("abc");
		checkout.phone.sendKeys("abc");
		checkout.email.sendKeys(getSaltString()+"@gmail.com");
		checkout.createaccount.click();
		checkout.passwordaccount.sendKeys("a");
		if (checkout.weakpass.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Biling Information", LogStatus.PASS,
					"Checkout Page Create Acc Weak Pass Error TC  : Passed");
		}
		else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Biling Information", LogStatus.PASS,
					"Checkout Page Create Acc Weak Pass Error  : Failed");
		}
		checkout.otherinfo.sendKeys("abc");
		Assert.assertTrue(checkout.Payment.isDisplayed());
		Assert.assertTrue(checkout.Label.isDisplayed());
		checkout.placeorder.click();

		if (checkout.Errors.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Biling Information", LogStatus.PASS,
					"Checkout Page Wronge Data Error TC  : Passed");
		}
		else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "Biling Information", LogStatus.PASS,
					"Checkout Page Wronge Data Error TC  : Failed");
		}

	}


	@AfterClass
	public static void teardown() {

		reports.endTest(test);
		reports.flush();
		driver.quit();

	}

}
