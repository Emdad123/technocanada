package Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;

import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

import java.util.regex.Pattern; 


public class HomeTest extends TechnoTutorBase
{
  //Home Page Test Scripts
  private static Home home;
  private static ExtentReports reports;
  private static ExtentTest test;

  //Report Intialization
  //Driver and URL initialization
  @BeforeClass
  private static void init() 
  {
	  initilization();
	  home = PageFactory.initElements(driver, Home.class); 
	  reports = ExtentFactory.getReporterInstance();
	  test = reports.startTest("Validate Home Page Test Cases");
  }
  
  
   @Test(priority = 0, enabled=true)
  void ValidateHomePageLoadingOnURL()
  {	
	  //Validate Home Page loading on URL access
	  try
	  {
	  		Assert.assertEquals(driver.getTitle(), "Techno Tutors � Personalized Tutoring Service");
	  		LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateHomePageLoadingOnURL", LogStatus.PASS, "Home Page loading on URL access TC  : Passed");
	  }
	  catch(Exception e)
	  {
		  	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateHomePageLoadingOnURL", LogStatus.FAIL, "Home Page loading on URL access TC  : Failed");
	  }
  }	
  	

  @Test(priority = 1, enabled=true)

  void ValidateLogoTC()
  {	
	  	//Validate Home Page Logo
		if(home.homePageLogo.isDisplayed())
		{
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateLogoTC", LogStatus.PASS, "Home Page Logo TC : Passed");
		}
		else
		{

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateLogoTC", LogStatus.FAIL, "Home Page Logo TC : Failed");
		}
   }	
  

  @Test(priority = 2 , enabled=true)
  void ValidateMenuLinksTest() 
  {
		//Verify Home Page link
	    if(isClickable(home.linkHome, driver))
	    {
	    	home.linkHome.click();
	    	Assert.assertEquals(driver.getTitle(), "Techno Tutors � Personalized Tutoring Service");
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestHomeLink", LogStatus.PASS, "Home Page Link TC : Passed");
	    }
	    else
	    {
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestHomeLink", LogStatus.FAIL, "Home Page Link TC : Failed");
	    }
	    //Verify About Us Page link
	    if(isClickable(home.linkaboutUs, driver))
	    {
	    	home.linkaboutUs.click();
	    	Assert.assertEquals(driver.getTitle(), "About Us � Techno Tutors");	
	    	System.out.println("");
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestAboutUsLink", LogStatus.PASS, "About Us Page Link TC : Passed");
	    	//verify Home link of About Us page redirect to Home page
	    	home.linkHome.click();
	    	test.log(LogStatus.PASS, "Page redirected to Home Page ");
	    }
	    else
	    {
	     	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestAboutUsLink", LogStatus.FAIL, "About Us Page Link TC : Failed");
	    }
		
		//Verify Facility page link
	    if(isClickable(home.linkFacility, driver))
	    {
	    	home.linkFacility.click();
	    	Assert.assertEquals(driver.getTitle(), "Facility � Techno Tutors");	
	       	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestFacilityLink", LogStatus.PASS, "Facility Page Link TC : Passed");
	    	//verify Home link of Facility page redirect to Home page
	    	home.linkHome.click();
	    	test.log(LogStatus.PASS, "Page redirected to Home Page ");
	    }
	    else
	    {
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestFacilityLink", LogStatus.FAIL, "Facility Page Link TC : Failed");
	    }
		
		//Verify Subject Areas page link
	    if(isClickable(home.linkSubjectAreas, driver))
	    {
	    	home.linkSubjectAreas.click();
	    	Assert.assertEquals(driver.getTitle(), "Subject Areas � Techno Tutors");	
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestSubjectAreasLink", LogStatus.PASS, "Subject Areas Page Link TC : Passed");
	    	//verify Home link of Subject Areas page redirect to Home page
	    	home.linkHome.click();
	    	test.log(LogStatus.PASS, "Page redirected to Home Page ");
	    }
	    else
	    {
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestSubjectAreasLink", LogStatus.FAIL, "Subject Areas Page Link TC : Failed");
	    }
		
		//Verify Contact Us page link
	    if(isClickable(home.linkContact, driver))
	    {
	    	home.linkContact.click();
	    	Assert.assertEquals(driver.getTitle(), "Contact � Techno Tutors");	
	       	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestContactUsLink", LogStatus.PASS, "Contact Us Page Link TC : Passed");
	    	//verify Home link of Contact Us page redirect to Home page
	    	home.linkHome.click();
	    	test.log(LogStatus.PASS, "Page redirected to Home Page ");
	    }
	    else
		{
	       	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestContactUsLink", LogStatus.FAIL, "Contact Us Page Link TC : Failed");
		}
		
		//Verify Cart page link
	    if(isClickable(home.linkCart, driver))
	    {
	    	home.linkCart.click();
	    	Assert.assertEquals(driver.getTitle(), "Cart � Techno Tutors");	
	    	System.out.println("Cart Page Link TC : Passed");
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestCartLink", LogStatus.PASS, "Cart Page Link TC : Passed");
	    	//verify Home link of Cart page redirect to Home page
	    	home.linkHome.click();
	    	test.log(LogStatus.PASS, "Page redirected to Home Page ");
	    }
	    else
		{
	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "ValidateMenuLinksTestCartLink", LogStatus.FAIL, "Cart Page Link TC : Failed");
		}
  } 
  
  @Test(priority = 3 , enabled=true)
   void SubjectSectionTest() 
  {
	  //Verify subject section is displayed or not
	  if(home.lblSubjectsSection.isDisplayed())
	  {
		  test.log(LogStatus.PASS, "Subjects Section Label : Passed");
	  }
	  else
	  {
		  test.log(LogStatus.FAIL, "Subjects Section Label : Failed");
	  }
	 //Validate MathMore Button
	  if((ValidateMathMore()).equalsIgnoreCase("Subject Areas � Techno Tutors"))
	  {
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestMath", LogStatus.PASS, "MoreButton:MathSubject:SubjectSection:HomePage TC : Passed");
		  driver.navigate().back();
	  }
	  else
	  {
		  System.out.println("MoreButton:MathSubject:SubjectSection:HomePage TC : Failed");
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestMath", LogStatus.FAIL, "MoreButton:MathSubject:SubjectSection:HomePage TC : Failed");
	  }
	  
	//Validate EnglishMore Button
	  if((ValidateEnglishMore()).equalsIgnoreCase("Subject Areas � Techno Tutors"))
	  {
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestEnglish", LogStatus.PASS, "MoreButton:EnglishSubject:SubjectSection:HomePage TC : Passed");
		  driver.navigate().back();
	  }
	  else
	  {
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestEnglish", LogStatus.FAIL, "MoreButton:EnglishSubject:SubjectSection:HomePage TC : Failed");
	  }
	  
	//Validate FrenchMore Button
	  if((ValidateFrenchMore()).equalsIgnoreCase("Subject Areas � Techno Tutors"))
	  {
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestFrench", LogStatus.PASS, "MoreButton:FrenchSubject:SubjectSection:HomePage TC : Passed");
		  driver.navigate().back();
	  }
	  else
	  {
		  LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectSectionTestFrench", LogStatus.FAIL, "MoreButton:FrenchSubject:SubjectSection:HomePage TC : Failed");
	  }
	 
  }

 @Test(priority = 4, enabled=true)
  void MandatoryFieldsTC()
  {	
	 	//Verify Name, Email and Subject of CONTACT US Section are manadatory fields 
	 	sendInquiry("", "", "", "");
	 	if(home.ERMessageName.isDisplayed())
	 	{
	 		LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCName", LogStatus.PASS, "Contact Us section : Mandatory Name Field TC : Passed");
	 	}
 	    else
 	    {
 	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCName", LogStatus.FAIL, "Contact Us section : Mandatory Name Field TC : Failed");
 	    	
 	    }
	 	
	 	if(home.ERMessageEmail.isDisplayed())
	 	{
	 		LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCEmail", LogStatus.PASS, "Contact Us section : Mandatory Email Field TC : Passed");
	 	}
 	    else
 	    {
 	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCEmail", LogStatus.FAIL, "Contact Us section : Mandatory Email Field TC : Failed");
 	    }
	 	if(home.ERMessageSubject.isDisplayed())
	 	{
	 		LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCSubject", LogStatus.PASS, "Contact Us section : Mandatory Subject Field TC : Passed");
	 	}
 	    else
 	    {
 	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCSubject", LogStatus.FAIL, "Contact Us section : Mandatory Subject Field TC : Failed");
 	    }
	 	
	 	if(home.ErrMessageMessage.isDisplayed())
	 	{
	 		LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCMessage", LogStatus.PASS, "Contact Us section : Mandatory Message Field TC : Passed");
	 	}
 	    else
 	    {
 	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCMessage", LogStatus.FAIL, "Contact Us section : Mandatory Message Field TC : Failed");
 	    }
	 	if(home.ErrMessageSend.isDisplayed())
	 	{
	 		LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCSend", LogStatus.PASS, "Contact Us section : Mandatory Fields TC : Passed");
	 	}
 	    else
 	    {
 	    	LogWithScreenshot.logStatusWithScreenShot(driver, test, "MandatoryFieldsTCSend", LogStatus.FAIL, "Contact Us section : Mandatory Fields TC : Failed");
 	    }
  }
	 
 
 @Test(priority = 5, enabled=true)
 void SendInquiryTC()
 {	
	 //Verify Send Message functionality
	 sendInquiry("Ganesh", "Ganesh123@gmail.com", "Inquiry", "Inquiry about course");
	 System.out.println(home.lblSuccessMessage.getText());
	 //System.out.println(home.txtName.getAttribute("value"));
	 //System.out.println(home.txtSubject.getText());
	 //System.out.println(home.txtMessage.getText());
	 if(home.lblSuccessMessage.isDisplayed())
	 {
		 LogWithScreenshot.logStatusWithScreenShot(driver, test, "SendInquiryTC", LogStatus.PASS, "Contact Us section : Send functionality TC : Passed");
	 }
	 else
	 {
		 LogWithScreenshot.logStatusWithScreenShot(driver, test, "SendInquiryTC", LogStatus.FAIL, "Contact Us section : Send functionality TC : Failed");
	 }
 }
 

 @Test(priority = 6, enabled=true)
 void VerifyEmailTC()
 {	
	 //Validate the email field of Contact Us section
	 sendInquiry("Ganesh", "Mani123@gmail.com", "Inquiry", "Inquiry about course");
	 try
	 {
	 if (isValidEmail(home.txtEmail.getText())==false && home.ERMessageWrongEmail.isDisplayed()) 
		{
        
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "VerifyEmailTC", LogStatus.PASS, "Contact Us section : Email Address Validation TC : Passed");

		}
	 }
	 catch(Exception e)
	 {
    /* else if(isValidEmail(home.txtEmail.getText()) && home.ERMessageWrongEmail.isDisplayed())
     {
     	 
     	System.out.print("Email Address Validation TC : Failed"); 
     }
     else if(isValidEmail(home.txtEmail.getText())==false && home.ERMessageWrongEmail.isDisplayed()==false)
     {
     	 
     	System.out.print("Email Address Validation TC : Failed"); 
     }*/
		 e.getMessage();
		 //LogWithScreenshot.logStatusWithScreenShot(driver, test, "VerifyEmailTC", LogStatus.FAIL, "Contact Us section : Email Address Validation TC : Failed");
	 }
 }

 
 @Test(priority = 7, enabled=true)
 void VerifyNameTC()
 {	
	 //Validate Name textbox of Contact Us section should have letters only 
	 String name ="Ganesh";
	 String regexString = "[A-Za-z]";//"^[A-Z+$]";
	 sendInquiry(name, "Mani123@gmail.com", "Inquiry", "Inquiry about course");
	
     if (name.matches("[" + regexString + "]+"))
     {
    	 LogWithScreenshot.logStatusWithScreenShot(driver, test, "VerifyNameTC", LogStatus.PASS, "Contact Us Section : Home Page : Name field accepting only letters TC : Passed");
     }
     else
     {
    	 LogWithScreenshot.logStatusWithScreenShot(driver, test, "VerifyNameTC", LogStatus.FAIL, "Contact Us Popup : Home Page : Name field accepting only letters TC : Failed");
     }
	 
 }
 
 @Test(priority = 8, enabled=true)
 void AboutUSandSubjectslinksofEducationLearning()
 {	
    	Actions action = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(home.JSlinkAbout));
		action.moveToElement(home.JSlinkAbout).click().build().perform();
		Assert.assertEquals(driver.getTitle(), "About Us � Techno Tutors");	
		LogWithScreenshot.logStatusWithScreenShot(driver, test, "AboutUSlinksofEducationLearning", LogStatus.PASS, "About Us link of Education Learning JS Section: Passed");
		home.linkHome.click();

		wait.until(ExpectedConditions.elementToBeClickable(home.JSlinkSubjects));
		action.moveToElement(home.JSlinkSubjects).click().build().perform();
		Assert.assertEquals(driver.getTitle(), "Subject Areas � Techno Tutors");
		LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectslinksofEducationLearning", LogStatus.PASS, "Subjects link of Education Learning JS Section: Passed");
		home.linkHome.click();
}
  
 
 //method for clicking element
 public boolean isClickable(WebElement el, WebDriver driver) 
 {
	try
	{
		WebDriverWait wait = new WebDriverWait(driver, 6);
		wait.until(ExpectedConditions.elementToBeClickable(el));
		return true;
	}
	catch(Exception e)
	{
		return false;
	}
 }
 
 //method for Math More button click
 public String ValidateMathMore()
 {
	 try 
	 {
		Thread.sleep(2000);
	 } 
	 catch (InterruptedException e) 
	 {
		e.printStackTrace();
	 }
	Actions action = new Actions(driver);
	action.moveToElement(home.btnMathMore).click().build().perform();
	try 
	{
		Thread.sleep(2000);
	} 
	catch (InterruptedException e) 
	{
		e.printStackTrace();
	}
	return driver.getTitle();	
 }   
 
 //method for English More button click
 public String ValidateEnglishMore()
 {
	 try 
	 {
		Thread.sleep(2000);
	 } 
	 catch (InterruptedException e) 
	 {
		e.printStackTrace();
	 }
	 Actions action = new Actions(driver);
	 action.moveToElement(home.btnEnglishMore).click().build().perform();
	 try 
	 {
		Thread.sleep(2000);
	 } 
	 catch (InterruptedException e) 
	 {
		e.printStackTrace();
	 }
	return driver.getTitle();		
 }
  
 //method for French More button click
 public String ValidateFrenchMore()
 {
	 try 
	 {
		Thread.sleep(2000);
	 } 
	 catch (InterruptedException e) 
	 {
		e.printStackTrace();
	 }
	 Actions action = new Actions(driver);
	 action.moveToElement(home.btnFrenchMore).click().build().perform();
	 try 
	 {
		Thread.sleep(2000);
	 } 
	 catch (InterruptedException e) 
	 {
		e.printStackTrace();
	 }
	 return driver.getTitle();			
 }
 
 //method for entering data to name, email, subject and message fields of Contact Us section/Popup
 void sendInquiry(String name, String email, String subject, String message) 
 {
	    home.txtName.clear();
		home.txtName.sendKeys(name);
		home.txtEmail.clear();
		home.txtEmail.sendKeys(email);
		home.txtSubject.clear();
		home.txtSubject.sendKeys(subject);
		home.txtMessage.clear();
		home.txtMessage.sendKeys(message);
		home.btnSend.click();
 }
 
 //method to validate Email Address format
 public static boolean isValidEmail(String email) 
 { 
     String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                         "[a-zA-Z0-9_+&*-]+)*@" + 
                         "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                         "A-Z]{2,7}$"; 
                           
     Pattern pat = Pattern.compile(emailRegex); 
     if (email == null) 
     {
         return false;
     }
     return pat.matcher(email).matches(); 
 } 
 
 //ending report
 //Closing Browser
 @AfterClass
 public static void teardown() 
 {
	reports.endTest(test);
	reports.flush();
	driver.close();
 }
	 
}