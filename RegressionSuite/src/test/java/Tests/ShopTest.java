package Tests;



import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.Cart;
import Pages.Home;
import Pages.Shop;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class ShopTest extends TechnoTutorBase {

	 private static Home home;
	 private static Shop shop;
	 private static Cart cart;
	 private static ExtentReports reports;
	 private static ExtentTest test;

	@BeforeClass
	public void init() 
	{
		initilization();
		home = PageFactory.initElements(driver, Home.class);
		shop = PageFactory.initElements(driver, Shop.class);
		cart = PageFactory.initElements(driver, Cart.class);
		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate Shop Page Test Cases");
		home.linkCart.click();                     
		cart.ReturntoShop.click();
	}
	
	// CONTACT US TITLE TEST
	@Test(priority = 1)
	public void checktitle() {
		String Title = driver.getTitle();
	    Assert.assertEquals(Title, "Products � Techno Tutors", "Title is Incorrect");
	}

	@Test(priority = 2)
	public void shop() {
		if (shop.ImageAcadamic.isDisplayed() == true & shop.PriceAcadamic.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Products", LogStatus.PASS,
					"Shop Images & price Academic Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Products", LogStatus.PASS,
					"Shop Images & price Academic Display  : Failed");
		}

		if (shop.ImageLearner.isDisplayed() == true & shop.PriceLearner.isDisplayed()) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Products", LogStatus.PASS,
					"Shop Images & price Learner Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Products", LogStatus.PASS,
					"Shop Images & price Learner Display  : Failed");
		}
		shop.AcadamicAddcart.click();
		shop.LearnerAddcart.click();
		shop.viewcart.click();
		String title = driver.getTitle();
		Assert.assertEquals(title, "Cart � Techno Tutors", "TITLE IS INCORRECT");
		driver.navigate().back();

	}

	@Test(priority = 4)
	public void AcademicPage() {

		shop.LinkAcadamic.click();
		if (shop.AcadamicPageImg.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Image", LogStatus.PASS,
					"Shop Page Academic Image Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Image", LogStatus.PASS,
					"Shop Page Academic Image Display  : Failed");
		}
		shop.AcadamicQuantity.clear();
		shop.AcadamicQuantity.sendKeys("2");
		shop.AddToCartBtn.click();

		if (shop.SuccessAddAlaertAcadamic.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Quantity", LogStatus.PASS,
					"Shop Page Academic Quantity Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Quantity", LogStatus.PASS,
					"Shop Page Academic QuantityDisplay  : Failed");
		}

		shop.Submit.click();
		Alert alert = driver.switchTo().alert();
		alert.dismiss();

		shop.stars.click();
		shop.Review.sendKeys("good");
		shop.Name.sendKeys("vijay");
		shop.Email.sendKeys(getSaltString()+"@gmail.com");
		shop.Submit.click();

		if (shop.SucessReview.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Review", LogStatus.PASS,
					"Shop Page Academic Review Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Academic Review", LogStatus.PASS,
					"Shop Page Academic Quantity Display  : Failed");
		}
	}

	@Test(priority = 5)
	public void LearnerPage() {

		driver.get("https://www.technotutors.ca/shop/");
		shop.LinkLearner.click();
		if (shop.LearnerPageImg.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Image", LogStatus.PASS,
					"Shop Page Learner Image Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Image", LogStatus.PASS,
					"Shop Page Learner Image Display  : Failed");
		}
		shop.LearnerQuantity.clear();
		shop.LearnerQuantity.sendKeys("2");
		shop.AddToCartBtn.click();

		if (shop.SuccessAddAlaertLearner.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Quantity", LogStatus.PASS,
					"Shop Page Learner Quantity Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Quantity", LogStatus.PASS,
					"Shop Page Learner Quantity Display  : Failed");
		}

		shop.Submit.click();
		Alert alert = driver.switchTo().alert();
		alert.dismiss();

		shop.stars.click();
		shop.Review.sendKeys("good");
		shop.Name.sendKeys("vijay");
		shop.Email.sendKeys(getSaltString()+"@gmail.com");
		shop.Submit.click();

		if (shop.SucessReview.isDisplayed() == true) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Review", LogStatus.PASS,
					"Shop Page Learner Review Display  : Passed");
		} else {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, " Shop Page Learner Review", LogStatus.PASS,
					"Shop Page Learner Quantity Display  : Failed");
		}

	}

	@AfterClass
	public void teardown() 
	{
		reports.endTest(test);
		reports.flush();
		driver.quit();
	}
	

}
