package Tests;

import java.util.Set;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.Home;
import Util.ExtentFactory;
import Util.LogWithScreenshot;
import Pages.Facility;

public class FacilityTest extends TechnoTutorBase
{
    //Facility Page Test Scripts
	private static Home home;
	private static Facility facility;
	private static ExtentReports reports;
	private static ExtentTest test;
	
	
	//Driver and URL initialization
	//Report Intialization
	@BeforeClass
	 private static void init() 
	 {
		 initilization();
		 home = PageFactory.initElements(driver, Home.class);
		 facility = PageFactory.initElements(driver, Facility.class); 
		 reports = ExtentFactory.getReporterInstance();
		 test = reports.startTest("Verify Facility Page Test Cases");
		  
	 }
	
	
	@Test(priority = 0, enabled=true)
	void ValidateContactUsButtonPresence()
	{	
		//Verify CONTACT US button should be displayed on Facility page
		home.linkFacility.click();
		Assert.assertEquals(driver.getTitle(), "Facility � Techno Tutors");
		if(facility.btnContactUS.isDisplayed() == true)
		{
			   
			 LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityValidateContactUsButtonPresence", LogStatus.PASS, "Contact Us Button Display :: Facility Page TC : Passed");
			    	
		}
		else
		{
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityValidateContactUsButtonPresence", LogStatus.FAIL, "Contact Us Button Display :: Facility Page TC : Failed");
		}
	}	
	
	@Test(priority = 1, enabled=true)
	void ValidateContactUsPopupSendInquiryTC()
	{	
		//Verify Send Message functionality CONTACT US  popup 		
		if(facility.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			facility.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for(String window : windowsID) 
			{
				if (!parentID.equals(window))
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				sendInquiry("Shiva", "shiva123@gmail.com", "FacultyInquiry", "Faculty Inquiry"); 
				if(facility.lblSuccessMessage.isDisplayed())
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityValidateContactUsPopupSendInquiryTC", LogStatus.PASS, "Contact Us Popup of Facility Page : Send functionality TC : Passed");
				}
				else
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityValidateContactUsPopupSendInquiryTC", LogStatus.FAIL, "Contact Us Popup of Facility Page :Send functionality TC: Failed");
				}
			}
			facility.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
		}
    }	
	 
	
	@Test(priority = 2, enabled=true)
	void VerifyNameTC()
	{	
		//Validate Name textbox of CONTACT US  popup  should have letters only 
		if(facility.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			facility.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				String name ="Kartik";
				String regexString = "[A-Za-z]";//"^[A-Z+$]";
				sendInquiry(name, "kartik123@gmail.com", "StartDateInquiry", "Inquiry about course start date");
		
				if (name.matches("[" + regexString + "]+"))
				{
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityVerifyNameTC", LogStatus.PASS, "Contact Us Popup : Facility Page : Name field accepting only letters TC : Passed"); 
				}
				else
				{

					LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityVerifyNameTC", LogStatus.FAIL, "Contact Us Popup : Facility Page : Name field accepting only letters TC : Failed"); 
				}
			}
			facility.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
		}
	 }
	
	
	 @Test(priority =3, enabled=true)
	 void VerifyEmailTC()
	 {	
		//Validate the email field of CONTACT US  popup 
		if(facility.btnContactUS.isDisplayed() == true)
		{								
			String parentID = driver.getWindowHandle();
			facility.btnContactUS.click();
			Set<String> windowsID = driver.getWindowHandles();
			for (String window : windowsID) 
			{
				if (!parentID.equals(window)) 
				{
					driver.switchTo().window(window);
					test.log(LogStatus.PASS, "ContactUs popup displayed");
				}
				sendInquiry("Gauri", "Gauri123gmailcom", "DurationInquiry", "Inquiry about course duration");
				try
				{
					if (HomeTest.isValidEmail(facility.txtEmail.getText())==false && facility.ERMessageWrongEmail.isDisplayed()) 
					{       
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityVerifyEmailTC", LogStatus.PASS, "Contact Us Popup : Facility Page : Email Address Validation TC : Passed"); 
					}
				}
				catch(Exception e)
				{
					e.getMessage();
					//LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityVerifyEmailTC", LogStatus.FAIL, "Contact Us Popup : Facility Page : Email Address Validation TC : Failed");
				}
			}
			facility.imgClose.click();
			test.log(LogStatus.PASS, "ContactUs popup closed");
			home.linkHome.click();
			test.log(LogStatus.PASS, "Page redirected to Home Page");
		}
	 }
	
	 @Test(priority = 4, enabled=true)
		void MandatoryFieldsTC()
	    {	
			//Verify Name, Email and Subject of Verify Name, Email and  Subject of CONTACT US  popup  are manadatory fields  are manadatory fields 
			if(facility.btnContactUS.isDisplayed() == true)
			{								
				String parentID = driver.getWindowHandle();
				facility.btnContactUS.click();
				Set<String> windowsID = driver.getWindowHandles();
				for (String window : windowsID) 
				{
					if (!parentID.equals(window)) 
					{
						driver.switchTo().window(window);
						test.log(LogStatus.PASS, "ContactUs popup displayed");
					}
					sendInquiry("", "", "", "");
					if(facility.ERMessageName.isDisplayed())
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCName", LogStatus.PASS, "Contact Us Popup : About Us Page : Mandatory Name Field TC : Passed");
					}
					else
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCName", LogStatus.FAIL, "Contact Us Popup : About Us Page : Mandatory Name Field TC : Failed");
		 	    	
					}
					if(facility.ERMessageEmail.isDisplayed())
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCEmail", LogStatus.PASS, "Contact Us Popup : Facility Page : Mandatory Email Field TC : Passed");
					}
					else
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCEmail", LogStatus.FAIL, "Contact Us Popup : Facility Page : Mandatory Email Field TC : Failed");
					}
					if(facility.ERMessageSubject.isDisplayed())
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCSubject", LogStatus.PASS, "Contact Us Popup : Facility Page : Mandatory Subject Field TC : Passed");
					}
					else
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCSubject", LogStatus.FAIL, "Contact Us Popup : Facility Page : Mandatory Subject Field TC : Failed");
					}
					if(facility.ErrMessageMessage.isDisplayed())
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCMessage", LogStatus.PASS, "Contact Us Popup : Facility Page : Mandatory Message Field TC : Passed");
					}
					else
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCMessage", LogStatus.FAIL, "Contact Us Popup : Facility Page : Mandatory Message Field TC : Failed");
					}
					if(facility.ErrMessageSend.isDisplayed())
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCSend", LogStatus.PASS, "Contact Us Popup : Facility Page : Mandatory Fields TC : Passed");
					}
					else
					{
						LogWithScreenshot.logStatusWithScreenShot(driver, test, "FacilityMandatoryFieldsTCSend", LogStatus.FAIL, "Contact Us Popup : Facility Page : Mandatory Fields TC : Failed");
					}
			 
				}
				facility.imgClose.click();
				test.log(LogStatus.PASS, "ContactUs popup closed");
			}
		}
	 //method for entering data to name, email, subject and message fields of Contact Us section/Popup
	 void sendInquiry(String name, String email, String subject, String message) 
	 {
		 facility.txtName.sendKeys(name);
		 facility.txtEmail.clear();
		 facility.txtEmail.sendKeys(email);
		 facility.txtSubject.clear();
		 facility.txtSubject.sendKeys(subject);
		 facility.txtMessage.clear();
		 facility.txtMessage.sendKeys(message);
		 facility.btnSend.click();
	 }
	 
	 //ending report
	 //Closing Browser
	 @AfterClass
	 public static void teardown() 
	 {
		 reports.endTest(test);
		 reports.flush();
		 driver.close();
	 }
		 
}
