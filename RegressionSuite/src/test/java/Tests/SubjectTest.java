package Tests;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;
import Pages.SubjectAreas;
import Util.ExtentFactory;
import Util.LogWithScreenshot;

public class SubjectTest extends TechnoTutorBase 
{
	public static ExtentReports reports;
	public static ExtentTest test;
	public static SubjectAreas sub;

	@BeforeClass
	public void setup() 
	{
		initilization();
		sub = PageFactory.initElements(driver, SubjectAreas.class);
		reports = ExtentFactory.getReporterInstance();
		test = reports.startTest("Validate SubjectArea Page Test Cases");
	}
	
	
	
	
	@Test(priority = 1, enabled = true, description = "Validating if landing on SubjectArea page")
	void subjectAreaPage() 
	{
		try {
			sub.subjectlinkHome.click();

			Assert.assertEquals(sub.pageTitle.getText(), "Subjects");

			LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectAreaPage", LogStatus.PASS,
					"Landed on SubjectArea Page TC  : Passed");
		} catch (Exception e) 
		{
			
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectAreaPage", LogStatus.FAIL,
					"Unable to land on SubjectArea Page TC  : Failed");
		}
	}
	
	
	
	
	@Test(priority = 2, enabled=true, description = "Book a Tutor link for Math in SubjectArea page")
	void subjectMath() {
		try {

			// scroll-down for Math subject
			JavascriptExecutor je = (JavascriptExecutor) driver;

			je.executeScript("arguments[0].scrollIntoView(true);", sub.mathTitle);

			if (sub.mathTitle.isDisplayed() == true && sub.mathTutor.isEnabled() == true) {

				sub.mathTutor.click();

				Assert.assertEquals(sub.BookTutorPage.getText(), "Book a Tutor");

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageMath", LogStatus.PASS,
						"Landed on SubjectArea Page TC  : Passed");
				Thread.sleep(4000);

				// mouse-over to Learner pkg text box on book-a-tutor page
				Actions tooltip = new Actions(driver);
				tooltip.moveToElement(sub.box).build().perform();
				Thread.sleep(2000);

				Assert.assertTrue(sub.hidden.getAttribute("value").contains("1610"));

				// tooltip.moveToElement(sub.hidden).build().perform();

				driver.navigate().back();

				// validate the backward navigation from book tutor page to subject area page.

				try { // inner try-catch
					Assert.assertEquals(sub.pageTitle.getText(), "Subjects");

					LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectAreaPageMath", LogStatus.PASS,
							"Landed on SubjectArea Page TC  for Math Subject : Passed");

				} catch (Exception e) {
					LogWithScreenshot.logStatusWithScreenShot(driver, test, "SubjectAreaPageMath", LogStatus.FAIL,
							"Unable to land on SubjectArea Page TC for Math Subject : Failed");
				}
				Thread.sleep(2000);
			} // ending of if-block here

		} // outer try-block ends here

		catch (Exception e) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageMath", LogStatus.FAIL,
					"Unable to land on BookTutor Page TC  : Failed");
		}

	}
	

	@Test(priority = 3, enabled=true, description = "Book a Tutor link for English in SubjectArea page")
	void subjectEnglish() {
		try {

			// scroll-down for English subject
			JavascriptExecutor je = (JavascriptExecutor) driver;

			je.executeScript("arguments[0].scrollIntoView(true);", sub.Englishtutor);

			Assert.assertEquals(sub.englishTitle.getText(), "English");

			if (sub.englishTitle.isDisplayed() == true && sub.Englishtutor.isEnabled() == true) {

				sub.Englishtutor.click();

				Assert.assertEquals(sub.BookTutorPage.getText(), "Book a Tutor");

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageEnglish", LogStatus.PASS,
						"Landed on SubjectArea Page TC for English Subject : Passed");
				driver.navigate().back();

				// validate the backward navigation from book tutor page to subject area page.
				Assert.assertEquals(sub.pageTitle.getText(), "Subjects");
				Thread.sleep(2000);
			}

		} catch (Exception e) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageEnglish", LogStatus.FAIL,
					"Unable to land on BookaTutor Page TC for English Subject : Failed");
		}

	}
	
	
	

	@Test(priority = 4,enabled=true, description = "Book a Tutor link for French in SubjectArea page")
	void subjectFrench() {
		try {

			// scroll-down for French subject
			JavascriptExecutor je = (JavascriptExecutor) driver;

			je.executeScript("arguments[0].scrollIntoView(true);", sub.FrenchTutor);
			Thread.sleep(2000);

			if (sub.FrenchTutor.isDisplayed() == true && sub.FrenchTutor.isEnabled() == true) {

				sub.FrenchTutor.click();

				Assert.assertEquals(sub.BookTutorPage.getText(), "Book a Tutor");

				LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageFrench", LogStatus.PASS,
						"navigating to Book a Tutor Page TC for French Subject  : Passed");
				driver.navigate().back();

				// validate the backward navigation from book tutor page to subject area page.
				Assert.assertEquals(sub.pageTitle.getText(), "Subjects");
				Thread.sleep(3000);
			}

		} catch (Exception e) {
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "BookaTutorPageFrench", LogStatus.FAIL,
					"Unable to navigate to Book a Tutor Page TC for French Subject  : Failed");
		}
	}
	
		
	

	@Test(priority = 5, enabled=true,description = "Contact Us button on SubjectArea page")
	void contact() 
	{
		try
		{

		// scroll-down for Contact Us button of Subject Areas page
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].scrollIntoView(true);", sub.contactUs);
		sub.contactUs.click();
		test.log(LogStatus.PASS, "ContactUs Page displayed");
		
		String parentID = driver.getWindowHandle();
		        
		Set<String> allwindows = driver.getWindowHandles();
		
		for(String child : allwindows)
		{
			if (!parentID.equalsIgnoreCase(child)) 
			{
				driver.switchTo().window(child);
				Assert.assertEquals(sub.contactUsTitle.getText(), "Contact Us");
				LogWithScreenshot.logStatusWithScreenShot(driver, test, "ContactUs Page", LogStatus.PASS,
						"Landed on ContacUs Page TC  : Passed");
				driver.close();
				test.log(LogStatus.PASS, "ContactUs Page closed");
			 }	
		
		}	
		driver.switchTo().window(parentID);
		System.out.println("Parent window :" + driver.getTitle());	
		test.log(LogStatus.PASS, "Page redirected to SubjectAreas Page");
		}
		catch(Exception e)
		{
			LogWithScreenshot.logStatusWithScreenShot(driver, test, "ContactUsPage", LogStatus.FAIL,
					"Unable to navigate to ContactUs page TC  : Failed");
		}

	}
	
	
	
	@AfterClass
	public void teardown() 
	{
		reports.endTest(test);
		reports.flush();
		driver.quit();
		
	}
}
