package Util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Base.TechnoTutorBase;

//TestScript for maintaining test case log with screenshot in Extent Report
public class LogWithScreenshot extends TechnoTutorBase
{
		static File destFile;
		public static void logStatusWithScreenShot(WebDriver driver, ExtentTest test, String fileName, LogStatus logStatus, String msg) 
		{
			destFile = new File("test-output/Reports/screenshots/" + fileName + ".JPEG");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			
			try
			{
				FileUtils.copyFile(scrFile, destFile);
				test.log(logStatus, msg + test.addScreenCapture(destFile.getAbsolutePath()));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
}
