package Util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.relevantcodes.extentreports.ExtentReports;

import Base.TechnoTutorBase;

//Test Script for Extent Report for one or more WebPages/TestCases/Classes.
public class ExtentFactory extends TechnoTutorBase
{	
		static ExtentReports extentReports;	
		static String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		public static String finalPath = "test-output/Reports/" + "Result_" + timestamp + ".html";

		//Factroy Method to get the same Reporter Instance 
		public static ExtentReports getReporterInstance()
		{	
			extentReports = new ExtentReports(finalPath, false);
			extentReports.loadConfig(new File("extent-config.xml"));
			extentReports
            .addSystemInfo("Host Name", "TechnoTutors")
            .addSystemInfo("Environment", "SIT-Automation Testing")
            .addSystemInfo("Selenium Java Version ", "3.12.0");
			
			return extentReports;		
		}
		
}


