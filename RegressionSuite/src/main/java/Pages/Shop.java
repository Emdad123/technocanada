package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Shop {
	
	public WebDriver driver;

	public Shop(WebDriver driver) {

		this.driver = driver;
	}

	
	// SHOP WEB ELEMENTS
	
	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/academic-package/']/img")
	public WebElement ImageAcadamic;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/academic-package/']/span/span")
	public WebElement PriceAcadamic;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1609']")
	public WebElement AcadamicAddcart;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/learner-package/']/img")
	public WebElement ImageLearner;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/learner-package/']/span/span")
	public WebElement PriceLearner;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1610']")
	public WebElement LearnerAddcart;

	@FindBy(how = How.XPATH, using = "(//a[@href='https://www.technotutors.ca/cart/'])[3]")
	public WebElement viewcart;

	// ACADAMIC PRODUCT PAGE

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/academic-package/']")
	public WebElement LinkAcadamic;

	@FindBy(how = How.XPATH, using = "//div[@id='product-1609']/div/figure/div/img")
	public WebElement AcadamicPageImg;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement AcadamicQuantity;

	@FindBy(how = How.XPATH, using = "//button[@name='add-to-cart']")
	public WebElement AddToCartBtn;

	@FindBy(how = How.XPATH, using = "//*[@id=\"post-1609\"]/div[1]/div")
	public WebElement SuccessAddAlaertAcadamic;

	// LEARNER PRODUCT PAGE
	
	

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/product/learner-package/']")
	public WebElement LinkLearner;

	@FindBy(how = How.XPATH, using = "//*[@id=\"product-1610\"]/div/figure/div/img")
	public WebElement LearnerPageImg;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement LearnerQuantity;

	@FindBy(how = How.XPATH, using = "//*[@id=\"post-1610\"]/div[1]/div")
	public WebElement SuccessAddAlaertLearner;

	// REVIEW
	
	@FindBy(how = How.XPATH, using = "//div[@class='comment-form-rating']/p/span/a[2]")
	public WebElement stars;

	@FindBy(how = How.XPATH, using = "//textarea[@id='comment']")
	public WebElement Review;

	@FindBy(how = How.XPATH, using = "//input[@id='author']")
	public WebElement Name;

	@FindBy(how = How.XPATH, using = "//input[@id='email']")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "//input[@id='submit']")
	public WebElement Submit;
	
	@FindBy(how = How.XPATH, using = "//li[@id='tab-title-reviews']/a")
	public WebElement SucessReview;
	


}
