package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Base.TechnoTutorBase;

public class Checkout extends TechnoTutorBase {
	
	// RETURNING CUSTOMER

		@FindBy(how = How.XPATH, using = "//a[@class='showlogin']")
		public WebElement ReturningCustomer;

		@FindBy(how = How.XPATH, using = "//input[@id='username']")
		public WebElement UserName;

		@FindBy(how = How.XPATH, using = "//input[@id='password']")
		public WebElement Password;

		@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']")
		public WebElement invallidemail;

		@FindBy(how = How.XPATH, using = "//input[@id='rememberme']")
		public WebElement RememberMe;

		@FindBy(how = How.XPATH, using = "//button[@name='login']")
		public WebElement send;

		@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/my-account/lost-password/']")
		public WebElement Forgetpass;

		@FindBy(how = How.XPATH, using = "//input[@id='user_login']")
		public WebElement forgetpassemail;

		@FindBy(how = How.XPATH, using = "//button[@value='Reset password']")
		public WebElement Resetpass;

		@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']")
		public WebElement ResetpassError;

		// COUPON FOR CHECKOUT PAGE

		@FindBy(how = How.XPATH, using = "//a[@class='showcoupon']")
		public WebElement couponlink;

		@FindBy(how = How.XPATH, using = "//input[@id='coupon_code']")
		public WebElement insertcoupon;

		@FindBy(how = How.XPATH, using = "//button[@name='apply_coupon']")
		public WebElement applycheckoutcoupon;

		@FindBy(how = How.XPATH, using = "//ul[@role='alert']")
		public WebElement couponerror;

		@FindBy(how = How.XPATH, using = "//ul[@class='woocommerce-error']/li")
		public WebElement wrongecouponerror;

		// BILLING DETAILS

		@FindBy(how = How.XPATH, using = "//input[@id='billing_first_name']")
		public WebElement Firstname;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_last_name']")
		public WebElement Lastname;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_company']")
		public WebElement Comapany;

		@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-input-wrapper']/strong")
		public WebElement Contry;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_address_1']")
		public WebElement Streetname;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_address_2']")
		public WebElement apparment;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_city']")
		public WebElement Town;

		@FindBy(how = How.XPATH, using = "//span[@id='select2-billing_state-container']")
		public WebElement Province;

		@FindBy(how = How.XPATH, using = "//input[@class='select2-search__field']")
		public WebElement InputProvince;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_postcode']")
		public WebElement postalcode;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_phone']")
		public WebElement phone;

		@FindBy(how = How.XPATH, using = "//input[@id='billing_email']")
		public WebElement email;

		@FindBy(how = How.XPATH, using = "//input[@id='createaccount']")
		public WebElement createaccount;

		@FindBy(how = How.XPATH, using = "//input[@id='account_password']")
		public WebElement passwordaccount;

		@FindBy(how = How.XPATH, using = "//div[@class='woocommerce-password-strength short']")
		public WebElement weakpass;

		@FindBy(how = How.XPATH, using = "//textarea[@id='order_comments']")
		public WebElement otherinfo;

		@FindBy(how = How.XPATH, using = "//td[@class='product-name']")
		public WebElement productname;

		@FindBy(how = How.XPATH, using = "//tr[@class='order-total']")
		public WebElement total;

		@FindBy(how = How.XPATH, using = "//div[@class='woocommerce-checkout-payment']")
		public WebElement Payment;

		@FindBy(how = How.XPATH, using = "//label[@for='payment_method_stripe']")
		public WebElement Label;

		@FindBy(how = How.XPATH, using = "//button[@id='place_order']")
		public WebElement placeorder;

		@FindBy(how = How.XPATH, using = "(//div[@class='woocommerce-notices-wrapper'])[1]")
		public WebElement Errors;

		public  Checkout() {
			
			 PageFactory.initElements(driver, this);
		}
	

}
