package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactUs 
{
	WebDriver driver;
	
	public ContactUs(WebDriver driver) 
	{
		 this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//ul[@id='menu-tt-menu-1']/li[5]/a")
	public WebElement contactuslink;

	@FindBy(how = How.XPATH, using = "//strong[text()='Contact Us']")
	public WebElement logocontactus;

	@FindBy(how = How.XPATH, using = "//*[text()='Techno Tutors']")
	public WebElement technotext;

	@FindBy(how = How.XPATH, using = "//*[text()=' 55 Town Centre Court, Suite 102 Toronto, ON M1P 4X4']")
	public WebElement adresstext;

	@FindBy(how = How.XPATH, using = "//*[text()='info@technotutors.com']")
	public WebElement emailtext;

	@FindBy(how = How.XPATH, using = "//*[text()=' 416-279-1947']")
	public WebElement phonenumtext;

	@FindBy(how = How.XPATH, using = "//input[@name='text-625']")
	public WebElement Name;

	@FindBy(how = How.XPATH, using = "//input[@name='email-982']")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "//input[@name='text-org']")
	public WebElement Subject;

	@FindBy(how = How.XPATH, using = "//*[@name='textarea-841']")
	public WebElement Message;

	@FindBy(how = How.XPATH, using = "//*[@class='wpcf7-form-control wpcf7-submit']")
	public WebElement Send;

  public void getInTouch(String name, String email, String subject, String message) {
		Name.clear();
		Name.sendKeys(name);
		Email.clear();
		Email.sendKeys(email);
		Subject.clear();
		Subject.sendKeys(subject);
		Message.clear();
		Message.sendKeys(message);
		Send.click();
	
		 }
}