package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


import Base.TechnoTutorBase;

public class Home extends TechnoTutorBase
{
	WebDriver localDriver;
	
	public Home(WebDriver driver) 
	{
		 localDriver = driver;
	}
	
	//Home Page 
	
	//Logo
	@FindBy(how=How.XPATH, using="(//a[@href='https://www.technotutors.ca'])[1]/img")
	public WebElement homePageLogo; 
	
	//Home Menu Link
	@FindBy(how=How.LINK_TEXT, using="Home")
	public WebElement linkHome; 
	
	//About Us Link
	@FindBy(how=How.LINK_TEXT, using="About Us")
	public WebElement linkaboutUs; 
	
	//Facility Link
	@FindBy(how=How.LINK_TEXT, using="Facility")
	public WebElement linkFacility; 
	
	//Subject Areas Link
	@FindBy(how=How.LINK_TEXT, using="Subject Areas")
	public WebElement linkSubjectAreas; 
	
	//Contact Us Link
	@FindBy(how=How.LINK_TEXT, using="Contact")
	public WebElement linkContact; 
	
	//Cart Link
	@FindBy(how=How.LINK_TEXT, using="Cart")
	public WebElement linkCart; 
	
	//SubjectSection Heading
	@FindBy(how=How.XPATH, using="//h2[text()='Subjects']")
	public WebElement lblSubjectsSection; 
	
	//Math :: More Button
	@FindBy(how=How.XPATH, using="//a[@href='/subject-areas/']//parent::div[contains(@class,'math-btn')]/a")
	public WebElement btnMathMore; 
	
	//English :: More Button
	@FindBy(how=How.XPATH, using="//a[@href='/subject-areas/']//parent::div[contains(@class,'eng-btn')]/a")
	public WebElement btnEnglishMore; 
	
	//French :: More Button
	@FindBy(how=How.XPATH, using="//a[@href='/subject-areas/']//parent::div[contains(@class,'fr-btn')]/a")
	public WebElement btnFrenchMore; 
	
	//Contact Us Section Heading
	@FindBy(how=How.XPATH, using="//h2[contains(text(),'CONTACT')]")
	public WebElement lblContactUSSection; 
	
	//Book a Tutor lable of Contact  Us  section
	@FindBy(how=How.XPATH, using="//h2[starts-with(text(),'BOOK')]")
	public WebElement lblBookTutor; 
    
	//Name Textbox of Contact  Us  section
	@FindBy(how=How.XPATH, using="//input[@name='text-625']")
	public WebElement txtName; 
	
	//Email Textbox of Contact  Us  section
	@FindBy(how=How.XPATH, using="//input[@name='email-982']")
	public WebElement txtEmail; 
	
	//Subject Textbox of Contact  Us  section
	@FindBy(how=How.XPATH, using="//input[@name='text-org']")
	public WebElement txtSubject; 
	
	//Message RichTextbox of Contact  Us  section
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']")
	public WebElement txtMessage; 
	
	//Send Button of Contact  Us  section
	@FindBy(how=How.XPATH, using="//input[@value='Send']")
	public WebElement btnSend;
	
	//Validation message for Name Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-625']//following-sibling::span")
	public WebElement ERMessageName;
	
	//Validation message for Email Textbox
	@FindBy(how=How.XPATH, using="//input[@name='email-982']//following-sibling::span")
	public WebElement ERMessageEmail;
	
	//Validation message for wrong Email 
	@FindBy(how=How.XPATH, using="//span[contains(text(),'e-mail')]")
	public WebElement ERMessageWrongEmail;
	
	//Validation message for Subject Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-org']//following-sibling::span")
	public WebElement ERMessageSubject;
	
	//Validation message for Message RichTextbox
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']//following-sibling::span")
	public WebElement ErrMessageMessage; 
	
	//Validation message for Send Button
	@FindBy(how=How.XPATH, using="//div[(contains(text(),'try'))][contains(@class,'wpcf7-validation-errors')]")
	public WebElement ErrMessageSend; 
	
	//Link for left arrow of javscript section
	@FindBy(how=How.XPATH, using="//div[contains(@class,'tp-leftarrow')]")
	public WebElement linkLeftArrow; 
	
	//Link for right arrow of javscript section
	@FindBy(how=How.XPATH, using="//div[contains(@class,'tp-rightarrow')]")
	public WebElement linkRightArrow; 
	
	//Techno Tutors lable of javscript section
	@FindBy(how=How.XPATH, using="//h1[starts-with(text(),'Techno')]")
	public WebElement JSlblTechnoTutors; 
	
	//Techno Tutors sublable of javscript section
	@FindBy(how=How.XPATH, using="//strong[contains(text(),'children')]")
	public WebElement JSlblMovingText; 
	
	//Education Lable of javscript section
	@FindBy(how=How.XPATH, using="//div[contains(text(),'Education')]")
	public WebElement JSlblEducation; 
	
	//Learn Courses Lable of javscript section
	@FindBy(how=How.XPATH, using="//div[contains(text(),'Learning')]")
	public WebElement JSlblLearnCourses; 
	
	//About Us Link of javscript section
	@FindBy(how=How.XPATH, using="//a[contains(text(),'ABOUT')]")
	public WebElement JSlinkAbout; 
	
	//Subjects Link of javscript section
	@FindBy(how=How.XPATH, using="//a[text()='SUBJECTS']")
	public WebElement JSlinkSubjects; 
	
	//Success message on inquiry submission
	@FindBy(how=How.XPATH, using="//form[@method='post']/div[contains(text(),'Thank')][@role=\"alert\"]")
	public WebElement lblSuccessMessage;
	
}




