package Pages;

import java.sql.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Cart {
	public WebDriver driver;

	public Cart(WebDriver driver) {

		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//ul[@id='menu-tt-menu-1']/li[6]/a")
	public WebElement cartlink;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca/shop/']")
	public WebElement ReturntoShop;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1609']")
	public WebElement Acadamiccart;

	@FindBy(how = How.XPATH, using = "//a[@href='/shop/?add-to-cart=1610']")
	public WebElement Learnercart;

	@FindBy(how = How.XPATH, using = "(//a[@href='https://www.technotutors.ca/cart/'])[3]")
	public WebElement viewcart;

	@FindBy(how = How.XPATH, using = "//input[@name='coupon_code']")
	public WebElement CouponCode;

	@FindBy(how = How.XPATH, using = "//button[@name='apply_coupon']")
	public WebElement ApplyCoupon;

	@FindBy(how = How.XPATH, using = "//*[@class='woocommerce-error']")
	public WebElement Error;

	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement Quantity;

	@FindBy(how = How.XPATH, using = "//button[@name='update_cart']")
	public WebElement UpdateCart;

	@FindBy(how = How.XPATH, using = "(//a[@class='remove'])[2]")
	public WebElement cancel;

	@FindBy(how = How.XPATH, using = "//td[@data-title='Subtotal']")
	public WebElement Subtotal;

	@FindBy(how = How.XPATH, using = "//a[@class='checkout-button button alt wc-forward']")
	public WebElement CheckOutButton;

		public void shop() {
		
		Acadamiccart.click();
		viewcart.click();
	}
}
