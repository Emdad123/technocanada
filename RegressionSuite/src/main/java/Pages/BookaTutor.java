package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Base.TechnoTutorBase;

public class BookaTutor extends TechnoTutorBase {

	WebDriver localDriver;

	public BookaTutor(WebDriver driver) {

		localDriver = driver;
	}

	// Finding XPath of SubjectAreas Tab/Link
	@FindBy(how = How.LINK_TEXT, using = "Subject Areas")
	public WebElement linkSubjectAreas;

	@FindBy(how = How.XPATH, using = "//a[@href='https://www.technotutors.ca'])[1]/img")
	public WebElement homePageLogo;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Subjects')]")
	public WebElement pageTitle;

	@FindBy(how = How.XPATH, using = "//h2[text()='Math']")
	public WebElement mathTitle;

	// Book a Tutor for Math

	// Finding XPath of 'Book a Tutor' link
	@FindBy(how = How.XPATH, using = "//h2[text()='Math']//parent::div[@class='wpb_wrapper']/div[2]/a")
	public WebElement linkBookaTutorM;
	
	@FindBy(how = How.XPATH, using = "//td[@class='product-remove']/a")
	public WebElement emptycart;
	
	
	// Learner Package

	// Displaying Learner Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Learner Package']")
	public WebElement titleLearnerPkgM;

	// Finding XPath of Learner Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement quantityLearnerM;

	// Finding XPath of Quantity Error
	@FindBy(how = How.XPATH, using = "//p[@class='cart-empty']")
	public WebElement quantityError;

	// Finding XPath of Cart Page Message
	@FindBy(how = How.XPATH, using = "//div[@class='woocommerce-message']")
	public WebElement messageCart;

	// Finding XPath of Learner 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@onclick='booktutor()']")
	public WebElement buttonAddtocartLearnerM;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalLearnerM;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$LearnerM;

	// Academic Package

	// Displaying Academic Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Academic Package']")
	public WebElement titleAcademicPkgM;

	// Finding XPath of Academic Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//*[@id=\"post-1501\"]/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/form/div/input")
	public WebElement quantityAcademicM;

	// Finding XPath of Academic 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement buttonAddtocartAcademicM;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalAcademicM;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$AcademicM;

	// Book a Tutor for English

	// Finding Link Text of 'Book a Tutor' link
	@FindBy(how = How.XPATH, using ="//h2[text()='English']//parent::div[@class='wpb_wrapper']/div[2]/a")
	public WebElement linkBookaTutorE;

	// Learner Package

	// Displaying Learner Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Learner Package']")
	public WebElement titleLearnerPkgE;

	// Finding XPath of Learner Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement quantityLearnerE;

	// Finding XPath of Learner 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@onclick='booktutor()']")
	public WebElement buttonAddtocartLearnerE;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalLearnerE;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$LearnerE;

	// Academic Package

	// Displaying Academic Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Academic Package']")
	public WebElement titleAcademicPkgE;

	// Finding XPath of Academic Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//*[@id=\"post-1501\"]/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/form/div/input")
	public WebElement quantityAcademicE;

	// Finding XPath of Academic 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement buttonAddtocartAcademicE;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalAcademicE;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$AcademicE;

	// Book a Tutor for French

	// Finding Link Text of 'Book a Tutor' link
	@FindBy(how = How.XPATH, using ="//h2[text()='French']//parent::div[@class='wpb_wrapper']/div[2]/a")
	public WebElement linkBookaTutorF;

	// Learner Package

	// Displaying Learner Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Learner Package']")
	public WebElement titleLearnerPkgF;

	// Finding XPath of Learner Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//input[@class='input-text qty text']")
	public WebElement quantityLearnerF;

	// Finding XPath of Learner 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@onclick='booktutor()']")
	public WebElement buttonAddtocartLearnerF;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalLearnerF;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$LearnerF;

	// Academic Package

	// Displaying Academic Package Title
	@FindBy(how = How.XPATH, using = "//h2[text()='Academic Package']")
	public WebElement titleAcademicPkgF;

	// Finding XPath of Academic Pkg 'Quantity' option
	@FindBy(how = How.XPATH, using = "//*[@id=\"post-1501\"]/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/form/div/input")
	public WebElement quantityAcademicF;

	// Finding XPath of Academic 'Add to cart' button
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement buttonAddtocartAcademicF;

	// Finding Partial Link Text of 'Total:' link
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "Total:")
	public WebElement TotalAcademicF;

	// Finding XPath of '$0.00' link
	@FindBy(how = How.XPATH, using = "//span[@class='woocommerce-Price-amount amount']")
	public WebElement link$AcademicF;

	// Finding Link Text of 'View Cart' button
	@FindBy(how = How.LINK_TEXT, using = "View Cart")
	public WebElement linkViewCart;

	@FindBy(how = How.XPATH, using = "//strong[@class='nd_options_color_white nd_options_font_size_60 nd_options_font_size_40_all_iphone nd_options_line_height_40_all_iphone nd_options_first_font']")
	public WebElement clickViewCart;

	

}
