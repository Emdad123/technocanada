package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Base.TechnoTutorBase;

public class SubjectAreas extends TechnoTutorBase {
	WebDriver driver;
	
	public SubjectAreas(WebDriver driver) {
		this.driver= driver;		
		}
	
	// SUBJECT AREA PAGE.
	@FindBy(how = How.XPATH, using = "//div[@class='nd_options_display_table']/div[1]/div/ul/li[4]")
	public WebElement subjectlinkHome;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Subjects')]")
	public WebElement pageTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@id='post-1501']/h1/strong")
	public WebElement BookTutorPage;	

	@FindBy(how = How.XPATH, using = "//h2[text()='Math']")
	public WebElement mathTitle;

	// book a tutor button for maths
	@FindBy(how = How.XPATH, using = "//div[@class='vc_btn3-container vc_btn3-right']/a")
	public WebElement mathTutor;

	// English title
	@FindBy(how = How.XPATH, using = "//h2[text()='English']")
	public WebElement englishTitle;

	// book a tutor button for English
	@FindBy(how = How.XPATH, using = "//a[@href='/book-a-tutor/']")
	public WebElement Englishtutor;
	
	@FindBy(how = How.XPATH, using = "//h2[text()='French']")
	public WebElement frenchTitle;

	// book a tutor button for French
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'post-1278')]/div[3]/div[2]/div/div/div[2]/a")
	public WebElement FrenchTutor;
	
	// contactUs title
	@FindBy(how = How.XPATH, using = "//strong[text()='Contact Us']")
	public WebElement contactUsTitle;

	// contactUs button
	@FindBy(how = How.XPATH, using = "//a[text()='Contact Us']")
	public WebElement contactUs;
	
	// contactUs button
	@FindBy(how = How.XPATH, using = "//input[@title='Qty']")
	public WebElement box;

	// hidden element in box
	@FindBy(how = How.XPATH, using = "//input[@name= 'add-to-cart' and @value='1610' and @type='hidden']")
	public WebElement hidden;

}
