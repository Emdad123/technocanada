package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Base.TechnoTutorBase;

public class Facility extends TechnoTutorBase
{
	WebDriver localDriver;
	
	public Facility(WebDriver driver) 
	{
		 localDriver = driver;
	}
	
	//Facility page
	
	//Facility Heading Label
	@FindBy(how=How.XPATH, using="//strong[text()='Facility']")
	public WebElement lblFacility; 

	//Contact Us Button	
	@FindBy(how=How.XPATH, using="//a[@href='#']/button")
	public WebElement btnContactUS; 
		
	//Contact Us Popup :: Contact Us Heading Label 
	@FindBy(how=How.XPATH, using="//div/h3[contains(text(),'CONTACT')]")
	public WebElement lblContactUS; 

	//Contact Us Popup :: Name Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-625']")
	public WebElement txtName; 
		
	//Contact Us Popup :: Email Textbox
	@FindBy(how=How.XPATH, using="//input[@name='email-982']")
	public WebElement txtEmail; 
		
	//Contact Us Popup :: Subject Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-org']")
	public WebElement txtSubject; 
		
	//Contact Us Popup :: Message RichTextbox
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']")
	public WebElement txtMessage; 
		
	//Contact Us Popup :: Send Button
	@FindBy(how=How.XPATH, using="//input[@value='Send']")
	public WebElement btnSend;
		
	//Contact Us Popup :: Validation Message :: Name Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-625']//following-sibling::span")
	public WebElement ERMessageName;
		
	//Contact Us Popup :: Validation Message :: Email Textbox
	@FindBy(how=How.XPATH, using="//input[@name='email-982']//following-sibling::span")
	public WebElement ERMessageEmail;
	
	//Validation message for wrong Email 
	@FindBy(how=How.XPATH, using="//span[contains(text(),'e-mail')]")
	public WebElement ERMessageWrongEmail;
		
	//Contact Us Popup :: Validation Message :: Subject Textbox
	@FindBy(how=How.XPATH, using="//input[@name='text-org']//following-sibling::span")
	public WebElement ERMessageSubject;
		
	//Contact Us Popup :: Validation Message :: Message RichTextbox
	@FindBy(how=How.XPATH, using="//*[@name='textarea-841']//following-sibling::span")
	public WebElement ErrMessageMessage; 
		
	//Contact Us Popup :: Validation Message :: Send Button
	@FindBy(how=How.XPATH, using="//div[(contains(text(),'try'))][contains(@class,'wpcf7-validation-errors')]")
	public WebElement ErrMessageSend; 
		
	//Contact Us Popup :: Close Button
	@FindBy(how=How.XPATH, using="//img[@alt='close']")
	public WebElement imgClose;	
	
	//Success message on inquiry submission
	@FindBy(how=How.XPATH, using="//form[@action='/facility/#wpcf7-f1601-p1275-o1']/div[5]")
	public WebElement lblSuccessMessage;
	
}
